import momentTZ from 'moment-timezone'
import { get } from 'lodash'

export const parseCurrency = (amount) => {
  let amountNumber

  if (amount == null) {
    return 0
  }

  if (typeof amount === 'string') {
    amountNumber = Number(amount)
  } else {
    amountNumber = amount
  }

  if (isNaN(amountNumber)) {
    return 0
  }

  return Math.round(amountNumber * Math.pow(10, 2)) / Math.pow(10, 2)
}

export const removeNonNumericString = (str) => {
  if (!str) {
    return ''
  }

  return str.replace(/\D+/g, '')
}

export const formatPhoneNumber = (phoneNumber) => {
  if (!phoneNumber) {
    return ''
  }

  const phoneNumberStr = phoneNumber + ''

  return removeNonNumericString(phoneNumberStr).replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2 - $3')
}

// Checks if given reward is currently valid
export const getIsRewardValid = ({
  isLoggedIn = false,
  rewardDetails,
  isFirstOrder = false,
  productsDetails,
  cartSubTotal,
  redeemedRewardCount = 0,
  userPointsWithPromoApplied = 0,
}) => {
  const ERROR = {
    NOT_LOGGED_IN: { valid: false, message: 'Please log in before redeeming rewards' },
    NOT_ENOUGH_POINTS: { valid: false, message: 'Not Enough Points' },
    FREE_REWARD_EMPTY_CART: { valid: false, message: 'Free Reward cannot be redeemed with empty cart' },
    DISCOUNT_EMPTY_CART: { valid: false, message: 'Discount cannot be redeemed with empty cart' },
    INVALID_PRODUCT: { valid: false, message: 'Product is Invalid' },
    PRODUCT_SOLDOUT: { valid: false, message: 'Product is temporarily Out Of Stock' },
    MAXIMUM_COUNT: { valid: false, message: 'You cannot redeem any more of this reward' },
    INVALID_FIELD: { valid: false, message: 'This Reward cannot be redeemed' },
    INVALID_COUNT: { valid: false, message: 'You must redeem 1 or more' },
    INVALID_SUBTOTAL: { valid: false, message: 'SubTotal cannot be negative' },
  }

  if (!isLoggedIn) {
    return ERROR.NOT_LOGGED_IN
  }

  if (redeemedRewardCount <= 0) {
    return ERROR.INVALID_COUNT
  }

  if (cartSubTotal < 0) {
    return ERROR.INVALID_SUBTOTAL
  }

  if (isFirstOrder && rewardDetails.requiredPoints === 0 && cartSubTotal <= 0) {
    return ERROR.FREE_REWARD_EMPTY_CART
  }

  if (rewardDetails.discountSubtotal > 0 && cartSubTotal <= 0) {
    return ERROR.DISCOUNT_EMPTY_CART
  }

  if (redeemedRewardCount > rewardDetails.maximumCount) {
    return ERROR.MAXIMUM_COUNT
  }

  if (rewardDetails.productId && !productsDetails[rewardDetails.productId]) {
    return ERROR.INVALID_PRODUCT
  }

  if (rewardDetails.productId && !getIsProductActive(productsDetails[rewardDetails.productId])) {
    return ERROR.PRODUCT_SOLDOUT
  }

  if (userPointsWithPromoApplied < 0) {
    return ERROR.NOT_ENOUGH_POINTS
  }

  return { valid: true }
}

// Checks if given reward is redeemable.
export const getIsRewardRedeemable = ({
  isLoggedIn = false,
  rewardDetails,
  isFirstOrder = false,
  productsDetails,
  cartSubTotal,
  redeemedRewardCount = 0,
  userPointsWithPromoApplied = 0,
}) => {
  return getIsRewardValid({
    isLoggedIn,
    rewardDetails,
    isFirstOrder,
    productsDetails,
    cartSubTotal,
    redeemedRewardCount: redeemedRewardCount + 1,
    userPointsWithPromoApplied: userPointsWithPromoApplied - rewardDetails.requiredPoints,
  })
}

// Parses reward info
export const parseRewardInfo = ({ rewardInfo, isFirstOrder = false, productsDetails = {}, count }) => {
  const { type, value, points, imageUrl } = rewardInfo

  const rewardDetails = {
    // Descriptions
    name: null,
    imageUrl,
    maximumCount: 100,

    // WITH
    requiredPoints: null,
    totalRequiredPoints: null,

    // FOR
    discountSubtotal: null,
    totalDiscountSubtotal: null,
    productId: null,
    price: null,
    rewardValue: null,
    totalRewardValue: null,
  }

  // Type Follows With/For/Condition pattern
  // What are you redeeming WITH, what are you redeeming FOR, and in what CONDITION can you redeem it.
  // For instance, Free/Product/Birthday will indicate free product on birthday
  switch (type) {
    case 'FreeFirstOrderOrPoints/Discount':
      {
        rewardDetails.name = `$${value} OFF`
        const discount = parseFloat(value) || 0
        rewardDetails.discountSubtotal = discount
        rewardDetails.totalDiscountSubtotal = discount * count
        rewardDetails.rewardValue = discount
        rewardDetails.totalRewardValue = discount * count

        if (isFirstOrder) {
          rewardDetails.requiredPoints = 0
          rewardDetails.totalRequiredPoints = 0
          rewardDetails.maximumCount = 1
        } else {
          rewardDetails.requiredPoints = points
          rewardDetails.totalRequiredPoints = points * count
        }
      }
      break
    case 'Points/Discount':
      {
        rewardDetails.name = `$${value} OFF`
        const discount = parseFloat(value) || 0
        rewardDetails.discountSubtotal = discount
        rewardDetails.totalDiscountSubtotal = discount * count
        rewardDetails.rewardValue = discount
        rewardDetails.totalRewardValue = discount * count

        rewardDetails.requiredPoints = points
        rewardDetails.totalRequiredPoints = points * count
      }
      break
    case 'FreeFirstOrderOrPoints/Product':
      {
        const productDetails = productsDetails[value]
        if (productDetails) {
          rewardDetails.productId = productDetails.id
          rewardDetails.name = productDetails.name
          rewardDetails.price = productDetails.price
          rewardDetails.rewardValue = productDetails.price
          rewardDetails.totalRewardValue = productDetails.price * count
        }

        if (isFirstOrder) {
          rewardDetails.requiredPoints = 0
          rewardDetails.totalRequiredPoints = 0
          rewardDetails.maximumCount = 1
        } else {
          rewardDetails.requiredPoints = points
          rewardDetails.totalRequiredPoints = points * count
        }
      }
      break
    case 'Points/Product':
      {
        const productDetails = productsDetails[value]
        if (productDetails) {
          rewardDetails.productId = productDetails.id
          rewardDetails.name = productDetails.name
          rewardDetails.price = productDetails.price
          rewardDetails.rewardValue = productDetails.price
          rewardDetails.totalRewardValue = productDetails.price * count
        }

        rewardDetails.requiredPoints = points
        rewardDetails.totalRequiredPoints = points * count
      }
      break
  }
  return rewardDetails
}

export const moment = (...params) => momentTZ(...params).tz('America/Vancouver')

export const getIsOpenForGivenTime = ({ hours, targetDate, time, waitTime }) => {
  const weekday = targetDate.weekday()

  // get time from hours
  const open = get(hours[weekday], 'open')
  const close = get(hours[weekday], 'close')

  if (!open || !close) {
    return false
  }

  const openMoment = moment(open)
  const closeMoment = moment(close)

  const openTime = moment(targetDate).hours(openMoment.hours()).minutes(openMoment.minutes()).seconds(0)
  const closeTime = moment(targetDate).hours(closeMoment.hours()).minutes(closeMoment.minutes()).seconds(0)

  const orderCloseMoment = moment(closeTime).subtract(waitTime, 'minutes')

  if (openTime.isSame(closeTime)) {
    return false
  }

  let todayOrderClose

  if (openTime.isBefore(closeTime)) {
    todayOrderClose = orderCloseMoment
  } else {
    todayOrderClose = moment(orderCloseMoment).add(1, 'days')
  }

  return time.isBetween(openTime, todayOrderClose)
}

export const getIsProductActive = (productDetails) => {
  if (!productDetails) {
    // Product doens't exist
    return false
  }

  // Since Dec 16 2018, active property is deprecated in favor of activeAfter
  const activeAfter = get(productDetails, 'activeAfter')
  if (activeAfter) {
    const activeAfterMoment = moment(activeAfter)
    if (moment().isAfter(activeAfterMoment)) {
      return true
    } else {
      return false
    }
  }

  const active = get(productDetails, 'active')
  // If active property doesn't exist, this means product is active
  if (active === undefined || active === true) {
    return true
  }
  if (active === false) {
    return false
  }

  // As a fallback
  return true
}

export const isCoordinateInDeliveryZone = (coordinate, polygon) => {
  const x = coordinate.lat
  const y = coordinate.lng
  let inside = false
  for (let i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
    const xi = parseFloat(polygon[i].lat)
    const yi = parseFloat(polygon[i].lng)
    const xj = parseFloat(polygon[j].lat)
    const yj = parseFloat(polygon[j].lng)
    const intersect = yi > y !== yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi
    if (intersect) inside = !inside
  }
  return inside
}
